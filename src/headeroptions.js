import { Avatar } from '@mui/material'
import React from 'react'
import './headeroptions.css'
function headeroptions({  avatar,Icon,title }) {
  return (
    <div className='headeroptions'>
     {Icon&& <Icon className ='headerOptions__icon'/>}
   {avatar&& (
    <Avatar className='headeropOptions__icon' src={avatar}/>
   )}
     <h3 className='headerOption_title'>{title}</h3>
  
    

    </div>

  )
}

export default headeroptions
