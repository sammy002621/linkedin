import React from 'react';
import './header.css'
import Header from './header'
import Sidebar from './sidebar'
import './App.css'
import Feed from'./feed'
function App() {
  return (
    <div className="App">
   

      <Header></Header>

      <div className="app_body">
        <Sidebar/>
        <Feed/>
      </div>
    </div>
  );
}

export default App;
