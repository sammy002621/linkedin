import firebase from 'firebase'


// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyC_3VOUR4nmwZmU3gxC0X_Mes8xbBzu_zU",
    authDomain: "linkedin-clone-de19b.firebaseapp.com",
    projectId: "linkedin-clone-de19b",
    storageBucket: "linkedin-clone-de19b.appspot.com",
    messagingSenderId: "882921629213",
    appId: "1:882921629213:web:38c8784ff963d9866288f4",
    measurementId: "G-8G6BREQSFD"
  };
const firebaseApp=firebase.initializeApp(firebaseConfig)
const db=firebaseApp.firestore();
const auth=firebase.auth()

export {db,auth}

  
